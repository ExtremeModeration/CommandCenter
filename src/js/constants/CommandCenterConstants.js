var keyMirror = require('keymirror');

module.exports = keyMirror({
  SEND_MESSAGE: null,
  RECEIVE_MESSAGE: null,
  RECEIVE_ACTION: null,
  VIEWER_JOINED: null,
  VIEWER_PARTED: null,
  CREATE_COMMAND: null,
  UPDATE_COMMAND: null,
  REMOVE_COMMAND: null
});
